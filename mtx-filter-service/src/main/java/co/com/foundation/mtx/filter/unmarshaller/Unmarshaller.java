package co.com.foundation.mtx.filter.unmarshaller;

import org.w3c.dom.Document;

public interface Unmarshaller<I,O> {

	Document unmarshall( I input );
	
	
}
