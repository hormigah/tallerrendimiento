package co.com.foundation.mtx.reporting.business;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import co.com.foundation.mtx.reporting.model.MaintenanceExecution;
import co.com.foundation.mtx.reporting.model.Operator;
import co.com.foundation.mtx.reporting.model.ReportDTO;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ReportApi {

	@PersistenceContext
	private EntityManager entityManager;

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ReportDTO generate() {

		ReportDTO reportDTO = new ReportDTO();
		
		TypedQuery<MaintenanceExecution> query = entityManager.createQuery("select me from MaintenanceExecution me LEFT JOIN FETCH me.operatorList", MaintenanceExecution.class);
		List<MaintenanceExecution> list = query.getResultList();
		
		Set<MaintenanceExecution> listSinDuplicar = new LinkedHashSet<MaintenanceExecution>(list);
		list.clear();
		list.addAll(listSinDuplicar);
		
		for (MaintenanceExecution me : list) {

			evaluateOperators(reportDTO, me.getOperatorList());
			reportDTO.addMaintenanceCount();
			
		}

		return reportDTO;
	}

	// --------------------------

	private void evaluateOperators(ReportDTO reportDTO, List<Operator> operators) {
		
		for (Operator o : operators) {

			reportDTO.addCost(o.getCharge());
			if( !reportDTO.getOperators().contains( o.getFullName() ) ) {
				reportDTO.getOperators().add( o.getFullName() );
			}
			
		}

	}
}
